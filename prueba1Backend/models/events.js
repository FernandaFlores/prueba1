var mysql_host = "localhost";
var Sequelize =  require ('sequelize');
var sequelize = new Sequelize('Database', 'root', 'PWD',{
	host: mysql_host,
	dialect: 'mysql'
});

{"lat": 20.680715, "lon":-103.442555}

var event = sequelize.define ('events',{
	"id"				:{type: Sequelize.INTEGER, primaryKey:true, autoIncrement: true},
	"place"				:{type: Sequelize.STRING},
	"location"			:{type: Sequelize.JSON},
	"datetime"			:{type: Sequelize.DATE},
	"max_guests"		:{type: Sequelize.INTEGER},
	"current_guests"	:{type: Sequelize.INTEGER},
	"name"				:{type: Sequelize.STRING},
	"admin"				:{type: Sequelize.INTEGER},
	"date_created"		:{type: Sequelize.DATE},
	"description"		:{type: Sequelize.STRING},
	"status"			:{type: Sequelize.STRING},
	"deadline"			:{type: Sequelize.DATE}
});

//var db = database.open();
//db.excute

event.sync();//nombre la variable
module.exports = event;